project_name = "jenkins-pipeline-dsl"
repo = "https://hvallejos@bitbucket.org/hvallejos/app.git"
repo_name = "app"

pipelineJob(project_name) {
    definition {
        triggers {
            scm('H/1 * * * *')
        }
        cpsScm {
            scm {
                git {
                    remote {
                        name(repo_name)
                        url(repo)
                    }
                }
                scriptPath("Jenkinsfile")
            }
        }
    }
}